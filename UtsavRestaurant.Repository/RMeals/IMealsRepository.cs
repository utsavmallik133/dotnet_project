using UtsavRestaurant.Repository.Domain;

namespace UtsavRestaurant.Repository.RMeals
{
public interface IMealsRepository : IGenericRepository<Meals>
{
}
}