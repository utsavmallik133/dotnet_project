using UtsavRestaurant.Repository.Domain;
namespace UtsavRestaurant.Repository.RMeals
{
public class MealsRepository : GenericRepository<Meals>, IMealsRepository
{
public MealsRepository(RestaurantContext dbContext)
{
DbContext = dbContext;
}
}
}