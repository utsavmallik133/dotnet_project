using UtsavRestaurant.Repository.Domain;

namespace UtsavRestaurant.Repository.RMenuItems
{
public interface IMenuItemsRepository : IGenericRepository<MenuItems>
{
}
}