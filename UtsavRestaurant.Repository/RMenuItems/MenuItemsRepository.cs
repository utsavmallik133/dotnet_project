using UtsavRestaurant.Repository.Domain;
namespace UtsavRestaurant.Repository.RMenuItems
{
public class MenuItemsRepository : GenericRepository<MenuItems>, IMenuItemsRepository
{
public MenuItemsRepository(RestaurantContext dbContext)
{
DbContext = dbContext;
}
}
}