using UtsavRestaurant.Repository.Domain;
namespace UtsavRestaurant.Repository.RStaff
{
public class StaffRepository : GenericRepository<Staff>, IStaffRepository
{
public StaffRepository(RestaurantContext dbContext)
{
DbContext = dbContext;
}
}
}