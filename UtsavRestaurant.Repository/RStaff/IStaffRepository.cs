using UtsavRestaurant.Repository.Domain;

namespace UtsavRestaurant.Repository.RStaff
{
public interface IStaffRepository : IGenericRepository<Staff>
{
}
}