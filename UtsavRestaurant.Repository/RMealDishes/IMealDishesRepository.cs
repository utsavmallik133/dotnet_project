using UtsavRestaurant.Repository.Domain;

namespace UtsavRestaurant.Repository.RMealDishes
{
public interface IMealDishesRepository : IGenericRepository<MealDishes>
{
}
}