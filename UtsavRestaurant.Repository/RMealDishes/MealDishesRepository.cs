using UtsavRestaurant.Repository.Domain;
namespace UtsavRestaurant.Repository.RMealDishes
{
public class MealDishesRepository : GenericRepository<MealDishes>, IMealDishesRepository
{
public MealDishesRepository(RestaurantContext dbContext)
{
DbContext = dbContext;
}
}
}