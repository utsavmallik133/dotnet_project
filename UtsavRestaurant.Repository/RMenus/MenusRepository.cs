using UtsavRestaurant.Repository.Domain;
namespace UtsavRestaurant.Repository.RMenus
{
public class MenusRepository : GenericRepository<Menus>, IMenusRepository
{
public MenusRepository(RestaurantContext dbContext)
{
DbContext = dbContext;
}
}
}