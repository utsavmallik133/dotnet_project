using UtsavRestaurant.Repository.Domain;

namespace UtsavRestaurant.Repository.RMenus
{
public interface IMenusRepository : IGenericRepository<Menus>
{
}
}