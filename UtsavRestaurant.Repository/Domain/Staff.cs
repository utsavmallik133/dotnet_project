using System;
namespace UtsavRestaurant.Repository.Domain
{
public class Staff
{
    public int id {get; set;}
    public string first_name {get; set;}
    public string last_name {get; set;}
    public string other_details {get; set;}


    public virtual StaffRoles StaffRoles {get; set; }
}
}