using System;
namespace UtsavRestaurant.Repository.Domain
{
public class Meals
{
public int id { get; set; }

public DateTime date_of_meal {get; set;}
public int cost_of_meal {get; set;}

public string other_details {get; set;}

public virtual Customer Customer {get; set;}

public virtual Staff Staff {get; set;}

}
}
