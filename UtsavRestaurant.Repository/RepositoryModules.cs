using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using UtsavRestaurant.Repository.RCustomer;
using UtsavRestaurant.Repository.RMenus;
using UtsavRestaurant.Repository.RMenuItems;
using UtsavRestaurant.Repository.RStaffRoles;
using UtsavRestaurant.Repository.RStaff;
using UtsavRestaurant.Repository.RMeals;
using UtsavRestaurant.Repository.RMealDishes;
namespace UtsavRestaurant.Repository
{
public static class RepositoryModule
{
public static void Register(IServiceCollection services, string connection,
string migrationsAssembly)
{
    services.AddDbContext<RestaurantContext>(options =>
    options.UseSqlServer(connection, builder =>
    builder.MigrationsAssembly(migrationsAssembly)));
    
    services.AddTransient<ICustomerRepository, CustomerRepository>();
    services.AddTransient<IMenusRepository, MenusRepository>();
    services.AddTransient<IMenuItemsRepository, MenuItemsRepository>();
    services.AddTransient<IMealsRepository, MealsRepository>();
    services.AddTransient<IStaffRepository, StaffRepository>();
    services.AddTransient<IStaffRolesRepository, StaffRolesRepository>();
    services.AddTransient<IMealDishesRepository, MealDishesRepository>();
    
}
}
}