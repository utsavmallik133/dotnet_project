using UtsavRestaurant.Repository.Domain;
namespace UtsavRestaurant.Repository.RStaffRoles
{
public class StaffRolesRepository : GenericRepository<StaffRoles>, IStaffRolesRepository
{
public StaffRolesRepository(RestaurantContext dbContext)
{
DbContext = dbContext;
}
}
}