using UtsavRestaurant.Repository.Domain;

namespace UtsavRestaurant.Repository.RStaffRoles
{
public interface IStaffRolesRepository : IGenericRepository<StaffRoles>
{
}
}