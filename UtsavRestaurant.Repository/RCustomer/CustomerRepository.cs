using UtsavRestaurant.Repository.Domain;
namespace UtsavRestaurant.Repository.RCustomer
{
public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
{
public CustomerRepository(RestaurantContext dbContext)
{
DbContext = dbContext;
}
}
}