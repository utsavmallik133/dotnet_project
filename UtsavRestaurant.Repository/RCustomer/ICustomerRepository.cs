using UtsavRestaurant.Repository.Domain;
namespace UtsavRestaurant.Repository.RCustomer
{
public interface ICustomerRepository : IGenericRepository<Customer>
{
}
}