using Microsoft.Extensions.DependencyInjection;
using UtsavRestaurant.Services.SCustomer;
using UtsavRestaurant.Services.SMealDishes;
using UtsavRestaurant.Services.SMeals;
using UtsavRestaurant.Services.SMenuItems;
using UtsavRestaurant.Services.SMenus;
using UtsavRestaurant.Services.SStaff;
using UtsavRestaurant.Services.SStaffRoles;

namespace UtsavRestaurant.Services
{
public static class ServicesModule
{
public static void Register(IServiceCollection services)
{
services.AddTransient<ICustomerService, CustomerService>();
services.AddTransient<IMenusService, MenusService>();
services.AddTransient<IMenuItemsService, MenuItemsService>();
services.AddTransient<IMealsService, MealsService>();
services.AddTransient<IStaffService, StaffService>();
services.AddTransient<IStaffRolesService, StaffRolesService>();
services.AddTransient<IMealDishesService, MealDishesService>();
}
}
}