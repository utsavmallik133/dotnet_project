using UtsavRestaurant.Repository.Domain;
using UtsavRestaurant.Repository.RMeals;
namespace UtsavRestaurant.Services.SMeals
{
public class MealsService : GenericService<Meals>, IMealsService
{
public MealsService(IMealsRepository mealsRepository) :
base(mealsRepository)
{
}
}
}