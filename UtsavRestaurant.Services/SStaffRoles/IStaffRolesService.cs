using UtsavRestaurant.Repository.Domain;
namespace UtsavRestaurant.Services.SStaffRoles
{
public interface IStaffRolesService : IGenericService<StaffRoles>
{
}
}