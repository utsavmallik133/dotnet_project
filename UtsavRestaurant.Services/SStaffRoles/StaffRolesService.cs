using UtsavRestaurant.Repository.Domain;
using UtsavRestaurant.Repository.RStaffRoles;
namespace UtsavRestaurant.Services.SStaffRoles
{
public class StaffRolesService : GenericService<StaffRoles>, IStaffRolesService
{
public StaffRolesService(IStaffRolesRepository staffRolesRepository) :
base(staffRolesRepository)
{
}
}
}