using UtsavRestaurant.Repository.Domain;
namespace UtsavRestaurant.Services.SMenus
{
public interface IMenusService : IGenericService<Menus>
{
}
}