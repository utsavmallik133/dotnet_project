using UtsavRestaurant.Repository.Domain;
using UtsavRestaurant.Repository.RMenus;
namespace UtsavRestaurant.Services.SMenus
{
public class MenusService : GenericService<Menus>, IMenusService
{
public MenusService(IMenusRepository menusRepository) :
base(menusRepository)
{
}
}
}