using UtsavRestaurant.Repository.Domain;
namespace UtsavRestaurant.Services.SStaff
{
public interface IStaffService : IGenericService<Staff>
{
}
}