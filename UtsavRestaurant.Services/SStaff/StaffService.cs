using UtsavRestaurant.Repository.Domain;
using UtsavRestaurant.Repository.RStaff;
namespace UtsavRestaurant.Services.SStaff
{
public class StaffService : GenericService<Staff>, IStaffService
{
public StaffService(IStaffRepository staffRepository) :
base(staffRepository)
{
}
}
}