using UtsavRestaurant.Repository.Domain;
using UtsavRestaurant.Repository.RMenuItems;
namespace UtsavRestaurant.Services.SMenuItems
{
public class MenuItemsService : GenericService<MenuItems>, IMenuItemsService
{
public MenuItemsService(IMenuItemsRepository menuItemsRepository) :
base(menuItemsRepository)
{
}
}
}