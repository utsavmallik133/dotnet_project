using UtsavRestaurant.Repository.Domain;
namespace UtsavRestaurant.Services.SMenuItems
{
public interface IMenuItemsService : IGenericService<MenuItems>
{
}
}