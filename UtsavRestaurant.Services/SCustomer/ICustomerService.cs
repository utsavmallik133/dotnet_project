using UtsavRestaurant.Repository.Domain;
namespace UtsavRestaurant.Services.SCustomer
{
public interface ICustomerService : IGenericService<Customer>
{
}
}