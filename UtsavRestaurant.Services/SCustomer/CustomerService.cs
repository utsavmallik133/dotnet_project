using UtsavRestaurant.Repository.Domain;
using UtsavRestaurant.Repository.RCustomer;
namespace UtsavRestaurant.Services.SCustomer
{
public class CustomerService : GenericService<Customer>, ICustomerService
{
public CustomerService(ICustomerRepository customerRepository) :
base(customerRepository)
{
}
}
}