using UtsavRestaurant.Repository.Domain;
using UtsavRestaurant.Repository.RMealDishes;
namespace UtsavRestaurant.Services.SMealDishes
{
public class MealDishesService : GenericService<MealDishes>, IMealDishesService
{
public MealDishesService(IMealDishesRepository mealDishesRepository) :
base(mealDishesRepository)
{
}
}
}