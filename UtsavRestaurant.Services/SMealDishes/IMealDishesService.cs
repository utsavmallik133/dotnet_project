using UtsavRestaurant.Repository.Domain;
namespace UtsavRestaurant.Services.SMealDishes
{
public interface IMealDishesService : IGenericService<MealDishes>
{
}
}